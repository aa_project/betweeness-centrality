/*
Copyright 2016 Rolando Brondolin, Marco Arnaboldi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <omp.h>
#include <stdio.h>
#include <fstream>
#include <algorithm>
#include <iostream>
#include "Graph.hpp"


void printArray(unsigned long long array[], unsigned long long size)
{
	for (unsigned long long i = 0; i < size; i++ ) {
        std::cout << array[i] << ' ';
    }
    std::cout << std::endl;
}


int main(int argc, char *argv[]) 
{



	if(argc != 2)
	{
		std::cout << "Command usage: " << argv[0] << " ./GRAPH_PATH" << std::endl;
		return 0;
	}


	//Graph *graph = new Graph(argv[1]);

	//std::cout << graph->getNumVertex(); << std::endl;  


	return 1;
}
