"""
Copyright 2016 Rolando Brondolin, Marco Arnaboldi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import subprocess
import sys

test_output = open("output_missing.txt", "w")

#
# fb-wosn-friends.edges hybrid
#

test_output.write("fb-wosn-friends.edges hybrid\n")


we = subprocess.Popen(['./work_efficient_hybrid', 'fb-wosn-friends.edges', '768', '512'], env={'OMP_NUM_THREADS' : '16'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo1"

we = subprocess.Popen(['./work_efficient_hybrid', 'fb-wosn-friends.edges', '768', '512'], env={'OMP_NUM_THREADS' : '8'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo2"

we = subprocess.Popen(['./work_efficient_hybrid', 'fb-wosn-friends.edges', '768', '512'], env={'OMP_NUM_THREADS' : '4'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo3"

we = subprocess.Popen(['./work_efficient_hybrid', 'fb-wosn-friends.edges', '768', '512'], env={'OMP_NUM_THREADS' : '2'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo4"

we = subprocess.Popen(['./work_efficient_hybrid', 'fb-wosn-friends.edges', '768', '512'], env={'OMP_NUM_THREADS' : '1'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo5"


#
# com_amazon
#
test_output.write("com_amazon we\n")

we = subprocess.Popen(['./we_parallel', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '16'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo1"

we = subprocess.Popen(['./we_parallel', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '8'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo2"

we = subprocess.Popen(['./we_parallel', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '4'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo3"

we = subprocess.Popen(['./we_parallel', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '2'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo4"

we = subprocess.Popen(['./we_parallel', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '1'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo5"


#
# Delaunay_n14
#
test_output.write("com_amazon ep\n")

we = subprocess.Popen(['./ep', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '16'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo1"
test_output.flush()

we = subprocess.Popen(['./ep', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '8'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo2"

we = subprocess.Popen(['./ep', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '4'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo3"

we = subprocess.Popen(['./ep', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '2'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo4"

we = subprocess.Popen(['./ep', 'amazon_new.edges'], env={'OMP_NUM_THREADS' : '1'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo5"



#
# amazon_new
#
test_output.write("com_amazon hybrid\n")

we = subprocess.Popen(['./work_efficient_hybrid', 'amazon_new.edges', '768', '512'], env={'OMP_NUM_THREADS' : '16'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo1"

we = subprocess.Popen(['./work_efficient_hybrid', 'amazon_new.edges', '768', '512'], env={'OMP_NUM_THREADS' : '8'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo2"

we = subprocess.Popen(['./work_efficient_hybrid', 'amazon_new.edges', '768', '512'], env={'OMP_NUM_THREADS' : '4'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo3"

we = subprocess.Popen(['./work_efficient_hybrid', 'amazon_new.edges', '768', '512'], env={'OMP_NUM_THREADS' : '2'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo4"

we = subprocess.Popen(['./work_efficient_hybrid', 'amazon_new.edges', '768', '512'], env={'OMP_NUM_THREADS' : '1'}, stderr=subprocess.PIPE)

for line in we.stderr:
	test_output.write(line)
	#print line
	if we.poll() != None:
		break
print "pippo5"

test_output.close()